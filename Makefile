all: build-gauche build-gambit build-cyclone build-loko

cgi-bin:
	mkdir -p cgi-bin

run-dev-server-lighttp: cgi-bin
	DOCUMENT_ROOT=$(shell pwd)/cgi-bin lighttpd -D -f lighttpd.conf

build-gauche: cgi-bin
	gosh build-standalone \
		-o cgi-bin/hello-gauche \
		src/hello.scm \
		retropikzel/cgi/v0-1-0/main.scm
	gosh build-standalone \
		-o cgi-bin/param-gauche \
		src/param.scm \
		retropikzel/cgi/v0-1-0/main.scm

build-gambit: cgi-bin
	gsc -:r7rs,search=. \
		-exe \
		-o cgi-bin/hello-gambit \
		src/hello.scm
	gsc -:r7rs,search=. \
		-exe \
		-o cgi-bin/param-gambit \
		src/param.scm

build-cyclone: cgi-bin
	cyclone src/hello.scm
	mv src/hello cgi-bin/hello-cyclone
	cyclone src/param.scm
	mv src/param cgi-bin/param-cyclone

build-loko:
	LOKO_LIBRARY_FILE_EXTENSIONS=.loko,.sld,.sls,.scm loko -std=r7rs --compile src/hello.scm
	mv src/hello cgi-bin/hello-loko
	LOKO_LIBRARY_FILE_EXTENSIONS=.loko,.sld,.sls,.scm loko -std=r7rs --compile src/param.scm
	mv src/param cgi-bin/param-loko

run-dev-server-apache:
	docker build . --tag scheme-cgi-dev
	docker run \
		-it \
		-p 3000:80 \
		scheme-cgi-dev

run-test-file-uploap:
	bash test/file-upload.sh

