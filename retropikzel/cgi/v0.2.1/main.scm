(define-library
  (retropikzel cgi v0.2.1 main)
  (import (scheme base)
          (scheme time)
          (scheme read)
          (scheme write)
          (scheme file)
          (scheme char)
          (scheme process-context))
  (export cgi-init
          cgi-apply-middleware
          cgi-get-header
          cgi-get-parameter
          cgi-get-cookie
          cgi-get-body)
  (begin
    (define encode-replacements (list (list " " "%20")
                                      (list " " "+")
                                      (list "!" "%21")
                                      (list "#" "%23")
                                      (list "$" "%24")
                                      (list "%" "%25")
                                      (list "&" "%26")
                                      (list "'" "%27")
                                      (list "(" "%28")
                                      (list ")" "%29")
                                      (list "*" "%2A")
                                      (list "+" "%2B")
                                      (list "," "%2C")
                                      (list "/" "%2F")
                                      (list ":" "%3A")
                                      (list ";" "%3B")
                                      (list "=" "%3D")
                                      (list "?" "%3F")
                                      (list "@" "%40")
                                      (list "[" "%5B")
                                      (list "]" "%5D")
                                      (list "<" "%3C")
                                      (list ">" "%3E")
                                      (list "\\" "%5C")
                                      (list "\"" "%22")
                                      (list "\n" "%0A")
                                      (list "\r" "%0D")))

    (define decode-replacements (map reverse encode-replacements))

    (define headers->string
      (lambda (headers)
        (apply string-append (map
                               (lambda (key-value)
                                 (string-append (car key-value) ": " (cdr key-value) "\r\n"))
                               headers))))

    (define get-replacement
      (lambda (key mode)
        (let ((r (if (string=? mode "encode")
                   (assoc key encode-replacements)
                   (assoc key decode-replacements))))
          (if r (car (cdr r)) key))))

    (define endecode
      (lambda (mode s)
        (if (not s)
          ""
          (letrec ((s-length (string-length s))
                   (looper
                     (lambda (i result)
                       (if (< i s-length)
                         (let ((key-length (if (and (string=? mode "decode")
                                                    (string=? (string-copy s i (+ i 1)) "%")
                                                    (> s-length (+ i 2)))
                                             3
                                             1)))
                           (looper (+ i key-length)
                                   (string-append result
                                                  (get-replacement
                                                    (string-copy s i (+ i key-length))
                                                    mode))))
                         result))))
            (looper 0 "")))))

    (define cgi-url-encode
      (lambda (str)
        (cond ((string? str) (endecode "encode" str))
              (else str))))

    (define cgi-url-decode
      (lambda (str)
        (cond ((string? str) (endecode "decode" str))
              (else str))))

    (define string-split
      (lambda (str mark)
        (let* ((str-l (string->list str))
               (res (list))
               (last-index 0)
               (index 0)
               (splitter (lambda (c)
                           (cond ((char=? c mark)
                                  (begin
                                    (set! res (append res (list (string-copy str last-index index))))
                                    (set! last-index (+ index 1))))
                                 ((equal? (length str-l) (+ index 1))
                                  (set! res (append res (list (string-copy str last-index (+ index 1)))))))
                           (set! index (+ index 1)))))
          (for-each splitter str-l)
          res)))

    (define split-http-parameters
      (lambda (body)
        (cond ((or (not (string? body))
                   (string=? "" body))
               (list))
              (else (let ((bodylist (string->list body)))
                      (map (lambda (p)
                             (cons (list-ref p 0)
                                   (if (> (length p) 1)
                                     (list-ref p 1)
                                     "")))
                           (map (lambda (x) (string-split x #\=))
                                (string-split (list->string bodylist)
                                              #\&))))))))

    (define get-paremeter
      (lambda (key parameters)
        (cond ((null? parameters) "")
              ((null? key) "")
              ((not (assoc key parameters)) "")
              (else (cdr (assoc key parameters))))))

    (define get-cookies
      (lambda ()
        (let ((cookie-string (get-environment-variable "HTTP_COOKIE")))
          (if cookie-string
            (split-http-parameters cookie-string)
            (list)))))

    (define read-until-eof
      (lambda (port result)
        (let ((c (read-bytevector 4000 port)))
          (if (eof-object? c)
            (utf8->string result)
            (read-until-eof port (bytevector-append result c))))))

    (define get-parameters
      (lambda (query-string)
        (split-http-parameters query-string)))

    (define request-get
      (lambda (request what key)
        (let ((pair (assoc key (cdr (assoc what request)))))
          (if pair (cgi-url-decode (cdr pair)) ""))))

    (define cgi-get-parameter
      (lambda (request key)
        (request-get request 'parameters (symbol->string key))))

    (define cgi-get-header
      (lambda (request key)
        (request-get request 'headers (symbol->string key))))

    (define cgi-get-cookie
      (lambda (request cookie)
        (request-get request 'cookies (symbol->string cookie))))

    (define cgi-get-body
      (lambda (request)
        (cdr (assoc 'body request))))

    (define get-filenames
      (lambda (headers)
        (if (not (assoc "CONTENT_TYPE" headers))
          (list)
          (let ((content-type (string-split (cdr (assoc "CONTENT_TYPE" headers)) #\;)))
            (if (and (> (length content-type) 0)
                     (string=? (list-ref content-type 0) "multipart/form-data"))
              (list content-type))))))

    (define string-filter
      (lambda (str filter)
        (let ((result (list)))
          (string-for-each
            (lambda (c)
              (if (filter c)
                (set! result (append result (list c)))))
            str)
          (list->string result))))

    (define cgi-init
      (lambda ()
        (let* ((headers (map (lambda (p)
                               (cons (car p)
                                     (cdr p)))
                             (get-environment-variables)))
               (content-type-pair (if (assoc "CONTENT_TYPE" headers)
                                    (assoc "CONTENT_TYPE" headers)
                                    (cons "Content-Type" "text/html")))
               (content-type-data (string-split (cdr content-type-pair) #\;))
               (content-type (list-ref content-type-data 0))
               (request-method (cdr (assoc "REQUEST_METHOD" headers)))
               (query-string (cdr (assoc "QUERY_STRING" headers)))
               (parameters (list))
               (body "")
               (files (list)))
          (cond ((and content-type-pair
                      (string=? content-type "multipart-form-data"))
                 (let* ((breaker (bytevector-u8-ref (string->utf8 "-") 0))
                        (tmp-folder "/tmp"))
                   (if (string=? content-type "multipart/form-data")
                     (letrec* ((boundary (string->utf8 (string-append (list-ref (string-split
                                                                                  (list-ref content-type-data 1) #\=) 1))))
                               (boundary-length (bytevector-length boundary))
                               (content (letrec ((looper (lambda (bytes result)
                                                           (if (eof-object? bytes)
                                                             result
                                                             (looper (read-bytevector 4000 (current-input-port))
                                                                     (bytevector-append result bytes))))))
                                          (looper (read-bytevector 4000 (current-input-port)) (bytevector))))
                               (content-length (bytevector-length content))
                               (content-mark 0)
                               (looper (lambda (index)
                                         (cond ((< index (- content-length 4))
                                                (if (and (= breaker (bytevector-u8-ref content index))
                                                         (= breaker (bytevector-u8-ref content (+ index 1)))
                                                         (equal? boundary (bytevector-copy content (+ index 2) (+ index 2 boundary-length))))
                                                  (let* ((part (bytevector-copy content content-mark index))
                                                         (part-length (bytevector-length part))
                                                         (part-port (open-input-bytevector part))
                                                         (part-headers-length 0)
                                                         (part-headers (letrec ((loop (lambda (line result)
                                                                                        (if (or (eof-object? line) (string=? line ""))
                                                                                          (map (lambda (p) (string-split p #\:)) result)
                                                                                          (begin
                                                                                            (set! part-headers-length (+ part-headers-length
                                                                                                                         (string-length line)
                                                                                                                         2))
                                                                                            (loop (read-line part-port)
                                                                                                  (append result (list line))))))))
                                                                         (loop (read-line part-port) (list)))))
                                                    (if (and (not (null? part-headers))
                                                             (assoc "Content-Disposition" part-headers))
                                                      (let* ((content-disposition
                                                               (map
                                                                 (lambda (str)
                                                                   (let ((split (string-split str #\=)))
                                                                     (cons (string-filter (list-ref split 0) (lambda (c) (not (char=? c #\space))))
                                                                           (if (= (length split) 2)
                                                                             (string-filter (list-ref split 1) (lambda (c) (not (char=? c #\"))))
                                                                             ""))))
                                                                 (string-split (car (cdr (assoc "Content-Disposition" part-headers))) #\;)))
                                                             (filename (assoc "filename" content-disposition)))
                                                        (if (not filename)
                                                          (set! parameters
                                                            (append parameters
                                                                    (list
                                                                      (cons (cdr (assoc "name" content-disposition))
                                                                            (utf8->string (bytevector-copy content
                                                                                                           (+ (+ content-mark part-headers-length) 2)
                                                                                                           (- index 2)))))))
                                                          (let* ((tmp-file-path (string-append tmp-folder "/" (cdr filename)))
                                                                 (tmp-file-port (open-binary-output-file tmp-file-path)))

                                                            (write-bytevector (bytevector-copy content
                                                                                               (+ (+ content-mark part-headers-length) 2)
                                                                                               (- index 2))
                                                                              tmp-file-port)
                                                            (set! files (append files (list
                                                                                        (cons (cdr (assoc "name" content-disposition))
                                                                                              tmp-file-path))))))
                                                        (set! content-mark index)))
                                                    (looper (+ index boundary-length)))
                                                  (looper (+ index 1))))))))
                       (looper 0)))))
                (else (let ((raw-body (if (string=? request-method "POST")
                                        (read-until-eof (current-input-port) (make-bytevector 0))
                                        "")))
                        (set! parameters (get-parameters (if (string=? request-method "POST")
                                                           raw-body
                                                           query-string)))
                        (if (string=? request-method "POST")
                          (set! body raw-body)))))
          (list (cons 'headers headers)
                (cons 'parameters parameters)
                (cons 'cookies (get-cookies))
                (cons 'body body)
                (cons 'files files)))))

    (define cgi-apply-middleware
      (lambda (middleware handler)
        (let ((request (cgi-init)))
          (call-with-current-continuation
            (lambda (k)
              (with-exception-handler
                (lambda (ex)
                  (if (get-environment-variable "SCHEME_CGI_DEBUG")
                    (begin
                      (display "Content-type: text/html")
                      (display "\r\n")
                      (display "\r\n")
                      (display "500 - Internal Server Error" )
                      (display "</br></br><b>Request</b></br></br>")
                      (display request )
                      (display "</br></br><b>Error message</b></br></br>")
                      (display (error-object-message ex))
                      (display "</br></br><b>Irritants</b></br></br>")
                      (display (error-object-irritants ex))
                      (display "</br></br><b>Error object</b></br></br>")
                      (write ex)))
                  (k 'exception))
                (lambda ()
                  (let ((response (handler request)))
                    (display response)
                    (newline)))))))))))
