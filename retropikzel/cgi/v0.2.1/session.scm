(define-library
  (retropikzel cgi v0-1-0 session)
  (import (scheme base)
          (scheme time)
          (scheme read)
          (scheme write)
          (scheme file)
          (scheme process-context)
          (srfi 27))
  (export session-init
          session-get
          session-set!)
  (begin

    (define session-folder
      (if (get-environment-variable "SCMSESSION_FILE_PATH")
        (get-environment-variable "SCMSESSION_FILE_PATH")
        "/tmp"))

    (define generate-unique-session-id
      (lambda ()
        (random-source-randomize! default-random-source)
        (let* ((session-id
                 (string-append "SCM_SESSION_ID-"
                                (number->string (random-integer 1000000))
                                "-"
                                (number->string (random-integer 1000000))
                                "-"
                                (number->string (random-integer 1000000))
                                "-"
                                (number->string (random-integer 1000000))))
               (session-file-path (string-append session-folder
                                                 "/"
                                                 session-id)))
          (if (file-exists? session-file-path)
            (generate-unique-session-id)
            (begin
              (with-output-to-file session-file-path
                                   (lambda ()
                                     (write (list (cons 'id session-id)))))
              session-id)))))

    (define session-init
      (lambda (request)
        (let* ((current-session-id (let ((session-id (assoc "SCM_SESSION_ID" (assoc 'cookies request))))
                                     (if session-id (cdr session-id) #f)))
               (new-session-id (cond
                                 ((or (not current-session-id)
                                      (not (file-exists? (string-append session-folder
                                                                        "/"
                                                                        current-session-id))))
                                  (generate-unique-session-id))
                                 (else current-session-id))))
          (append request (list (cons "SCM_SESSION_ID" new-session-id))))))

    (define session-set!
      (lambda (request key value)
        (let* ((session-id (let ((session-id (assoc "SCM_SESSION_ID" request)))
                             (if session-id (cdr session-id) #f)))
               (session-file-path
                 (string-append session-folder
                                "/"
                                (if session-id
                                  session-id
                                  "SCMSESSION-NONEXISTENT"))))
          (if (file-exists? session-file-path)
            (let ((session-content
                    (with-input-from-file session-file-path
                                          (lambda () (read)))))
              (let ((new-session-content
                      (if (assoc key session-content)
                        (map (lambda (p)
                               (if (eq? (car p) key)
                                 (cons key value)
                                 p))
                             session-content)
                        (append session-content
                                (list (cons key value))))))
                (with-output-to-file
                  session-file-path
                  (lambda ()
                    (write new-session-content)))))))))

    (define session-get
      (lambda (request key)
        (let* ((session-id (let ((session-id (assoc "SCM_SESSION_ID" request)))
                             (if session-id (cdr session-id) #f)))
               (session-file-path
                 (string-append session-folder
                                "/"
                                (if session-id session-id "NONEXISTENT"))))
          (if (file-exists? session-file-path)
            (begin
              (let ((session-content
                      (with-input-from-file session-file-path
                                            (lambda () (read)))))
                (if (assoc key session-content)
                  (cdr (assoc key session-content))
                  "")))
            ""))))))


