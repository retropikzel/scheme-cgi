(import (scheme base)
        (scheme read)
        (scheme write)
        (retropikzel cgi v0-1-0 main))

(display "Content-Type: text/html")
(display "\r\n")
(display "\r\n")

(define request (cgi-init))

(define test (cgi-get-parameter request 'test))
(display "Param 'test' value is: ")
(display test)

