(import (scheme base)
        (scheme read)
        (scheme write)
        (retropikzel cgi v0-0-0 cgi))

(define request (cgi-init))
(display "Content-type: text/html")
(display "\r\n")
(display "\r\n")
(write request)
(display 
  "
<form method=POST action=/cgi-bin/post.scm>
    <input type=text name=username value=testuser></input>
    <input type=password name=password1 value=testpass></input>
    <input type=password name=password2 value=testpass></input>
    <input type=submit value=Register></input>
</form>")

(display "Username: ")
(write (cgi-get-parameter request "username"))
