(import (scheme base)
        (scheme read)
        (scheme write)
        (retropikzel cgi))

(define request (cgi-init))
(cgi-session-init request)


(cgi-session-set! request 'test "LOL2")

(cgi-response request
              200
              (list (cons "Content-type" "text/html"))
              (cgi-session-get request 'test))

