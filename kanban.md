# Ideas
## Show error on page if environment variable is set
Read environment variable when starting and if SCHEME_CGI_DEBUG is set then show errors on page.
## Support for scheme-http-server eval
It should work exactly the same as normal CGI scripts. The library should handle everything behind the scenes.
## Delete temporary files at the end of cgi execution
## Move sessions into its own library
This way they can be used with scheme-scgi too
## Add file upload support
# To Do
## Add documentation
