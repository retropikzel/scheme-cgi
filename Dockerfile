FROM debian:bullseye
RUN apt-get update && apt-get install -y apache2 build-essential wget libck-dev make
RUN a2enmod cgi
RUN wget https://github.com/justinethier/cyclone-bootstrap/archive/refs/tags/v0.35.0.tar.gz && tar -xf v0.35.0.tar.gz
RUN cd cyclone-bootstrap-0.35.0 && make && make install
RUN wget https://github.com/shirok/Gauche/releases/download/release0_9_12/Gauche-0.9.12.tgz && tar -xf Gauche-0.9.12.tgz
RUN cd Gauche-0.9.12 && ./configure && make && make install
RUN wget https://gambitscheme.org/latest/gambit-v4_9_4.tgz && tar -xf gambit-v4_9_4.tgz
RUN cd gambit-4.9.4 && ./configure && make && make install
RUN wget https://scheme.fail/releases/loko-0.12.1.tar.gz && tar -xf loko-0.12.1.tar.gz
RUN cd loko-0.12.1 && make && make install
RUN apt-get install -y libssl-dev 
COPY Makefile /var/lib
WORKDIR /var/lib
RUN make all
ENTRYPOINT ["bash", "-c", "apachectl -D FOREGROUND & tail -f /var/log/apache2/error.log & bash"]
