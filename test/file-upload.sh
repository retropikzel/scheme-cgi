#!/usr/env bash

UPLOAD_URL=127.0.0.1:3000/cgi-bin/file-upload.scm

curl -v \
    -F file1=@test/test.txt \
    -F 'test1=foobar' \
    -F file2=@test/test.png \
    -F 'test2=barfoo' \
    -F file3=@test/hedgehog.jpg \
    --trace-ascii /tmp/trace.log \
    ${UPLOAD_URL}

