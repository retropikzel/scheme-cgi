(import (scheme base)
        (scheme write)
        (retropikzel cgi v0-1-0 main))

(display "Content-type: text/html")
(display "\r\n")
(display "\r\n")

(define request (cgi-init))

(display "Hello")
(display "</br>")
(display (cgi-get-parameter request 'test))
